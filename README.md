# Pre-Commit tool
Is a tool that helps us configuring git hooks. [See more](https://pre-commit.com/)

## Quick Start
Ensure that we have pip installed on our machine and run the command below.

### 1. Install pre-commit

``` bash
pip install pre-commit
```

pre-commit --version should show you what version you're using
``` bash
$ pre-commit --version
pre-commit 2.20.0
```

### 2. Add a pre-commit configuration
create a file named `.pre-commit-config.yaml`
you can generate a very basic configuration using 

``` bash
pre-commit sample-config
```

this example uses a formatter for python code, however pre-commit works for any programming language
other supported hooks are available [here](https://pre-commit.com/hooks.html)
``` yaml
repos:
-   repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v2.3.0
    hooks:
    -   id: check-yaml
    -   id: end-of-file-fixer
    -   id: trailing-whitespace
-   repo: https://github.com/psf/black
    rev: 21.12b0
    hooks:
    -   id: black
```

### 3. Install the git hook scripts
run pre-commit install to set up the git hook scripts
``` bash
$ pre-commit install
pre-commit installed at .git/hooks/pre-commit
now pre-commit will run automatically on git commit!
```

### 4. (optional) Run against all the files
it's usually a good idea to run the hooks against all of the files when adding new hooks (usually pre-commit will only run on the changed files during git hooks)
```bash
pre-commit run --all-files
```

## How to use
Running `git commit` command will automatically trigger all the commands bound with pre-commit hook.

### Black example code
``` python
def is_unique(
			s
			):
	s = list(s
				)
	s.sort()


	for i in range(len(s) - 1):
		if s[i] == s[i + 1]:
			return 0
	else:
		return 1


if __name__ == "__main__":
	print(
		is_unique(input())
		)
```
